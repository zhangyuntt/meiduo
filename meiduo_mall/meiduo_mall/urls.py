"""meiduo_mall URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^', include('apps.index.urls', namespace="index")),
    url(r'^', include('apps.users.urls', namespace='users')),  # users应用的url配置
    url(r'^', include('apps.verifications.urls', namespace='apps.verifications')),
    url(r'^', include('apps.areas.urls', namespace="areas")),
    url(r'^', include('apps.goods.urls', namespace="goods")),
    url(r'^', include('apps.shoppingcart.urls', namespace="shoppingcart")),
    url(r'^', include('apps.orders.urls', namespace="orders")),
    url(r'^', include('apps.payment.urls', namespace="payment")),
    url(r'^meiduo_admin/', include('apps.meiduo_admin.urls')),
]
