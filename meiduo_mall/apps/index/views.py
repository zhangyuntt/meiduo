from pprint import pprint

from django.http import HttpResponse
# from django.shortcuts import render
from django.shortcuts import render
from django.views import View

from apps.goods.models import GoodsChannel
from apps.goods.utils import get_fenlei
from apps.index.models import ContentCategory


class IndexView(View):
    def get(self, request):
        """
        主页
        :param request:
        :return:
        """
        # return HttpResponse("我是 主页 ....")
        categories_dict = get_fenlei()
        # 渲染模板的上下文
        contents = {}
        content_categories = ContentCategory.objects.all()
        for temp in content_categories:
            # print(temp.key)
            contents[temp.key] = temp.content_set.filter(status=True).order_by('sequence')
        # pprint(contents)
        context = {
            'categories': categories_dict,
            'contents': contents,
        }

        return render(request, 'base_index.html', context)
