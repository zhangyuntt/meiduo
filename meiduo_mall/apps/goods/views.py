import json
from datetime import date
from pprint import pprint

from django import http
from django.core.paginator import Paginator, EmptyPage
from django.http import HttpResponseNotFound, JsonResponse
from django.shortcuts import render
from django.views import View
from django_redis import get_redis_connection

from apps.goods.models import GoodsCategory, SKU, GoodsVisitCount
from apps.goods.utils import get_breadcrumb, get_fenlei
from apps.orders.models import OrderGoods
from utils import constants
from utils.response_code import RETCODE
from utils.view import LoginRequiredJSONMixin


class ListView(View):
    """商品列表页"""

    def get(self, request, category_id, page_num):
        """提供商品列表页"""
        # 判断category_id是否正确
        try:
            category = GoodsCategory.objects.get(id=category_id)
        except GoodsCategory.DoesNotExist:
            return HttpResponseNotFound('GoodsCategory does not exist')

        # 查询商品频道分类
        categories = get_fenlei()

        # 查询面包屑导航
        breadcrumb = get_breadcrumb(category)
        # 接收sort参数
        sort = request.GET.get('sort', 'default')
        if sort == "price":
            # 安装价格排序
            sort_field = 'price'
        elif sort == 'hot':
            # 按照销量由高到低
            sort_field = '-sales'
        else:
            # 'price'和'sales'以外的所有排序方式都归为'default'
            sort = 'default'
            sort_field = 'create_time'
        # 渲染页面
        skus = SKU.objects.filter(spu__category3=category, is_launched=True).order_by(sort_field)
        # 创建分页器：每页N条记录
        paginator = Paginator(skus, constants.GOODS_LIST_LIMIT)
        # 获取每页商品数据
        try:
            page_skus = paginator.page(page_num)
        except EmptyPage:
            # 如果page_num不正确，默认给用户404
            return http.HttpResponseNotFound('empty page')
        # 获取列表页总页数
        total_page = paginator.num_pages

        # 渲染页面
        context = {
            'categories': categories,
            'breadcrumb': breadcrumb,
            'category': category,
            'sort': sort,  # 排序字段
            'page_skus': page_skus,  # 分页后数据
            'total_page': total_page,  # 总页数
            'page_num': page_num,  # 当前页码
        }
        return render(request, 'base_list.html', context)


class HotGoodsView(View):
    """热销产品排行榜"""

    def get(self, request, category_id):
        # 根据销量倒序
        skus = SKU.objects.filter(category_id=category_id, is_launched=True).order_by('-sales')[:2]
        # 序列化
        hot_skus = []
        for sku in skus:
            hot_skus.append({
                'id': sku.id,
                'default_image_url': sku.default_image,
                'name': sku.name,
                'price': sku.price,
            })

        return JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK', 'hot_skus': hot_skus})


class DetailView(View):
    """商品详情页"""

    def get(self, request, sku_id):
        """提供商品详情页"""
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return render(request, '404.html')
        # 查询商品频道分类
        categories = get_fenlei()
        # 查询面包屑导航
        category = sku.spu.category3
        breadcrumb = get_breadcrumb(category)
        # 取出所有的spu下的sku
        spu_specs_dict = {}
        all_skus = sku.spu.sku_set.all()
        # print(all_skus)
        # print("*"*30)
        for sku_temp in all_skus:
            one_sku_specs = sku_temp.specs.all()
            # print(one_sku_specs)
            for one_sku_spec in one_sku_specs:
                if (one_sku_spec.spec.name, one_sku_spec.option.value) not in spu_specs_dict:
                    spu_specs_dict[(one_sku_spec.spec.name, one_sku_spec.option.value)] = {sku_temp.id}
                else:
                    spu_specs_dict[(one_sku_spec.spec.name, one_sku_spec.option.value)].add(sku_temp.id)

        # pprint(spu_specs_dict)
        # demo数据
        # {('内存', '256GB'): {8, 4, 6},
        #  ('内存', '64GB'): {3, 5, 7},
        #  ('颜色', '深空灰'): {5, 6},
        #  ('颜色', '金色'): {3, 4},
        #  ('颜色', '银色'): {8, 7}}

        for op, val in spu_specs_dict.items():
            # print(op, val)
            if sku.id in val:
                set_temp = val - {sku.id}  # 例如 {8, 6}
                # print(op,"op")
                spu_specs_dict[op] = {sku.id}  # 例如{4}
                for op_2, val_2 in spu_specs_dict.items():
                    # print(op_2)
                    for sku_id in set_temp:
                        # print(sku_id)
                        if sku_id in val_2:
                            spu_specs_dict[op_2] = {sku_id}
                            # pprint(spu_specs_dict)
                            # print("-"*30)

        # pprint(spu_specs_dict)
        # demo数据
        # {('内存', '256GB'): {4},
        #  ('内存', '64GB'): {3},
        #  ('颜色', '深空灰'): {6},
        #  ('颜色', '金色'): {4},
        #  ('颜色', '银色'): {8}}

        spu_options = {}
        for op, val in spu_specs_dict.items():
            if op[0] not in spu_options:
                spu_options[op[0]] = [(op[1], val.pop())]
            else:
                spu_options[op[0]].append((op[1], val.pop()))

        # pprint(spu_options)
        # demo数据
        # {'内存': [('64GB', 3), ('256GB', 4)], '颜色': [('金色', 4), ('深空灰', 6), ('银色', 8)]}

        # 渲染页面
        context = {
            'categories': categories,
            'breadcrumb': breadcrumb,
            'sku': sku,
            'specs': spu_options,
        }
        return render(request, 'detail.html', context)


class UserBrowseHistory(LoginRequiredJSONMixin, View):
    """用户浏览记录"""

    def post(self, request):
        """保存用户浏览记录"""
        json_dict = json.loads(request.body.decode())
        sku_id = json_dict.get('sku_id')
        try:
            SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return http.HttpResponseBadRequest('SKU不存在')
        # 保存用户的浏览数据
        redis_conn = get_redis_connection('history')
        pl = redis_conn.pipeline()
        user_id = request.user.id

        # 先去重
        pl.lrem('history_%s' % user_id, 0, sku_id)
        # 存储
        pl.lpush('history_%s' % user_id, sku_id)
        # 截取
        pl.ltrim('history_%s' % user_id, 0, 4)
        # 执行管道
        pl.execute()
        # 响应结果
        return http.JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK'})

    def get(self, request):
        """获取用户浏览记录"""
        # 获取redis数据库中的记录
        redis_conn = get_redis_connection('history')
        sku_ids = redis_conn.lrange('history_%s' % request.user.id, 0, -1)
        # 根据sku_ids列表数据，查询出商品的sku信息
        skus = []
        for sku_id in sku_ids:
            sku = SKU.objects.get(id=sku_id)
            skus.append({
                'id': sku.id,
                'name': sku.name,
                'default_image_url': sku.default_image,
                'price': sku.price
            })
        return http.JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK', 'skus': skus})


class GoodsCommentView(View):
    """订单商品评价信息"""

    def get(self, request, sku_id):
        # 获取被评价的订单商品信息
        order_goods_list = OrderGoods.objects.filter(sku_id=sku_id, is_commented=True)[:30]
        # 序列化
        comment_list = []
        for order_goods in order_goods_list:
            username = order_goods.order.user.username
            comment_list.append({
                'username': username[0] + '***' + username[-1] if order_goods.is_anonymous else username,
                'comment': order_goods.comment,
                'score': order_goods.score,
            })
        return http.JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK', 'comment_list': comment_list})


class DetailVisitView(View):
    def post(self, request, category_id):
        try:
            gvc = GoodsVisitCount.objects.get(category_id=category_id)
        except:
            GoodsVisitCount.objects.create(
                category_id=category_id,
                count=1
            )
        else:
            gvc.count += 1
            gvc.date = date.today()
            gvc.save()
        return http.JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK'})
