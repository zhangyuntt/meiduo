
from django.conf.urls import url

from apps.orders import views

urlpatterns = [
    url(r'^orders/settlement/$', views.OrderSettlementView.as_view(), name="place_order"),
    url(r'^orders/commit/$', views.OrderCommitView.as_view(), name="order_commit"),
    url(r'^orders/success/$', views.OrderSuccessView.as_view(), name="order_success"),
    url(r'^orders/comment/$', views.OrderCommentView.as_view(), name="order_comment"),
]