from django.conf.urls import url

# from .views import IndexView
from apps.shoppingcart import views

urlpatterns = [
    url(r'^carts/$', views.CartsView.as_view(), name="cart"),
    url(r'^carts/selection/$', views.CartsSelectAllView.as_view(), name='selectall'),
    url(r'^carts/simple/$', views.CartsSimpleView.as_view(), name='simple'),
]
