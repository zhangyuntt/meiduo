from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser
from django.utils import timezone

from apps.goods.models import GoodsVisitCount
from apps.meiduo_admin.serializers.statistical import GoodsVisitSerializer
from apps.users.models import User


class UserTotalCountView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        """
        获取网站总用户数:
        1. 获取网站总用户数量
        2. 返回应答
        """
        # 1. 获取网站总用户数量
        now_date = timezone.now()
        count = User.objects.count()

        # 2. 返回应答
        response_data = {
            # date: 只返回`年-月-日`
            'date': now_date.date(),
            'count': count
        }

        return Response(response_data)


class UserDayIncrementView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        """
        获取日增用户数量:
        1. 获取日增用户数量
        2. 返回应答
        """
        # 1. 获取日增用户数量
        now_date = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        count = User.objects.filter(date_joined__gte=now_date).count()

        # 2. 返回应答
        response_data = {
            'date': now_date.date(),
            'count': count
        }
        return Response(response_data)


class UserDayActiveView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        """
        获取日活用户量:
        1. 获取日活用户量
        2. 返回应答
        """
        # 1. 获取日活用户量
        now_date = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        count = User.objects.filter(last_login__gte=now_date).count()

        # 2. 返回应答
        response_data = {
            'date': now_date.date(),
            'count': count
        }

        return Response(response_data)


class UserDayOrdersView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        """
        获取日下单用户数量：
        1. 获取日下单用户数量
        2. 返回应答
        """
        # 1. 获取日下单用户数量
        now_date = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        count = User.objects.filter(orders__create_time__gte=now_date).distinct().count()

        # 2. 返回应答
        response_data = {
            'date': now_date.date(),
            'count': count
        }

        return Response(response_data)


class UserMonthCountView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        """
        获取近30天每日新增用户数据
        """
        # 1. 获取近30天每日新增用户数据
        # 结束时间
        now_date = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        # 起始时间: now_date - 29天
        begin_date = now_date - timezone.timedelta(days=29)

        # 当前日期
        current_date = begin_date

        # 新增用户的数量
        count_list = []

        while current_date <= now_date:
            # 次日时间
            next_date = current_date + timezone.timedelta(days=1)
            # 统计当天的新增用户数量
            count = User.objects.filter(date_joined__gte=current_date, date_joined__lt=next_date).count()

            count_list.append({
                'count': count,
                'date': current_date.date()
            })

            current_date += timezone.timedelta(days=1)

        # 2. 返回应答
        return Response(count_list)
class GoodsDayView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        """
        获取当日分类商品访问量：
        1. 查询获取当日分类商品访问量
        2. 将查询数据序列化并返回
        """
        # 1. 查询获取当日分类商品访问量
        # 当前日期
        now_date = timezone.now().date()

        goods_visit = GoodsVisitCount.objects.filter(date=now_date)

        # 2. 将查询数据序列化并返回
        serializer = GoodsVisitSerializer(goods_visit, many=True)
        return Response(serializer.data)




