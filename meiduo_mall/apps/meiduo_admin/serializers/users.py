from django.utils import timezone
from rest_framework import serializers

from apps.users.models import User


class AdminAuthSerializer(serializers.ModelSerializer):
    """管理员序列化器类"""
    username = serializers.CharField(
        label='用户名')  # 如果不进行定义，那么会导致在进行序列化器 校验时按照User模型类中的username的约束进行，而此时又是post提交方式，所以如果没有此行，会出现 {"username":["A user with that username already exists."]}
    token = serializers.CharField(label='JWT Token', read_only=True)  # User模型类是没有token属性的，所以需要额外添加

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'token')

        extra_kwargs = {
            'password': {
                'write_only': True  # 这里设置密码只能够进行反序列化而不能序列化。否则的话会将密码放到json中返回 用户会看到，这样不安全
            }
        }

    def validate(self, attrs):
        # 获取username和password
        username = attrs['username']
        password = attrs['password']

        # 进行用户名和密码校验
        try:
            user = User.objects.get(username=username, is_staff=True)  # is_staff 1表示管理员，可以修改数据库得到
        except User.DoesNotExist:
            raise serializers.ValidationError('用户名或密码错误1')
        else:
            # 校验密码
            if not user.check_password(password):
                raise serializers.ValidationError('用户名或密码错误2')

            # 给attrs中添加user属性，保存登录用户
            attrs['user'] = user

        return attrs

    def create(self, validated_data):
        # 获取登录用户user
        user = validated_data['user']

        # 设置最新登录时间
        user.last_login = timezone.now()
        user.save()

        # 服务器生成jwt token, 保存当前用户的身份信息
        from rest_framework_jwt.settings import api_settings

        # 组织payload数据的方法
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        # 生成jwt token数据的方法
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        # 组织payload数据
        payload = jwt_payload_handler(user)
        # 生成jwt token
        token = jwt_encode_handler(payload)

        # 给user对象增加属性，保存jwt token的数据
        user.token = token

        return user
