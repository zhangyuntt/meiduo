from django.conf.urls import url

from . import views

urlpatterns = [

    url(r'^register/$', views.RegisterView.as_view(), name='register'),  # 注册
    url(r'^usernames/(?P<username>[a-zA-Z0-9_]{5,20})/count/$', views.UsernameCountView.as_view(), name='user_count'),
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view(), name='mobile_count'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^usercenter/$', views.UserCenterView.as_view(), name='usercenter'),  # 用户中心
    url(r'^address/$', views.AddressView.as_view(), name='address'),  # 用户中心
    url(r'^password/$', views.ChangePasswordView.as_view(), name='password'),  # 用户中心
    url(r'^orders/info/(?P<page_num>\d+)/$', views.UserOrderInfoView.as_view(), name='orders'
        ),  # 用户中心
]
